#!/bin/bash

### Flatpak installer ###

source ./bash_colors

priv="sudo"

# Installers
echo -e "${BLUE}Flatpak setup...${NC}"
mkdir -p /home/$USER/Games
flatpak remote-delete fedora
flatpak remote-add --if-not-exists flathub https://flathub.org/repo/flathub.flatpakrepo
flatpak remotes
$priv cp ../.config/my_configs/flatpak-system-update.service /etc/systemd/system/
$priv cp ../.config/my_configs/flatpak-system-update.timer /etc/systemd/system/
systemctl enable flatpak-system-update.timer --now
flatpak install flathub \
                        com.brave.Browser \
                        net.lutris.Lutris \
                        com.valvesoftware.Steam \
                        org.videolan.VLC \
                        org.vim.Vim \
                        org.gnome.Rhythmbox3 \
                        org.gnome.Loupe \
                        org.gimp.GIMP \
                        org.libreoffice.LibreOffice \
                        org.keepassxc.KeePassXC \
                        im.riot.Riot \
                        org.godotengine.Godot \
                        org.mozilla.firefox \
                        com.spotify.Client \
                        com.github.tchx84.Flatseal \
                        com.vscodium.codium \
                        com.protonvpn.www \
                        com.mojang.Minecraft \
                        com.gitlab.davem.ClamTk \
                        org.gnome.gedit \
                        org.libretro.RetroArch \
                        com.play0ad.zeroad \
                        com.unity.UnityHub \
                        org.kde.okular \
                        com.github.jeromerobert.pdfarranger \
                        com.github.muriloventuroso.pdftricks \
                        net.codeindustry.MasterPDFEditor
flatpak override --user --filesystem=/home/$USER/Games com.valvesoftware.Steam
echo -e "${GREEN}Flatpaks Installed${NC}"
