#!/bin/bash

# USBGuard activation script

source ./bash_colors

echo -e "${BLUE}Generating Default Policy...${NC}"
usbguard generate-policy > /etc/usbguard/rules.conf
if grep "allow id" /etc/usbguard/rules.conf 
    then
    echo -e "${GREEN}Default Policy Generated${NC}"
else
    echo -e "${RED}Default Policy Not Generated...."
    echo -e "Please Generate a Default Policy${NC}"
    exit 1
fi
echo
echo -e "${BLUE}IPCAllowedUsers${NC}"
grep IPCAllowedUsers /etc/usbguard/usbguard-daemon.conf
echo
echo -e "${BLUE}Is "root" an Allowed User and Has a Default Policy been Generated?${NC}"
select policy in Yes No
do 
    case $policy in 
        Yes)
            systemctl enable usbguard.service
            systemctl start usbguard.service
            break;;
        No)
            echo -e  "${RED}Default Policy Not Generated...."
            echo -e "Please Generate a Default Policy!${NC}"
            exit 1;;
        *)
            echo -e "${RED}Invalid Input${NC}"
            exit 1;;

    esac
done
echo -e "${GREEN}USB Guard Enabled${NC}"
