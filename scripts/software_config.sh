#!/bin/bash

# Package configs

source ./bash_colors


echo -e "${BLUE}Generating Configs...${NC}"
mkdir -p "$HOME/Games"
mkdir -p "$HOME/.config"
mkdir -p "$HOME/Applications"
mkdir -p "$HOME/Virtual_Machines"
mkdir -p "$HOME/.local/bin"
mkdir -p "$HOME/.local/share/fonts/"
mkdir -p "$HOME/.vim/plugged" "$HOME/.vim/undodir"
cp -r ../wallpapers "$HOME/Pictures"
cp ../.config/my_configs/.bashrc "$HOME/.bashrc"
cp ../.config/my_configs/.toolboxrc "$HOME/.toolboxrc"
cp ../.config/my_configs/.vimrc "$HOME/.vimrc"
cp ../.config/my_configs/.exclude_list_rsync.txt "$HOME/.exclude_list_rsync.txt"
cp -r ../.config/* "$HOME/.config"
curl -fLo $HOME/.vim/autoload/plug.vim --create-dirs \
    https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
vim -es -u $HOME/.vimrc -i NONE -c "PlugInstall" -c "qa"
git clone https://github.com/nwg-piotr/autotiling.git
cp ./autotiling/autotiling/main.py "$HOME/.local/bin/autotiling"
chmod +x $HOME/.local/bin/autotiling
cp -r ../.local/share/fonts/NerdFonts "$HOME/.local/share/fonts/"
echo -e "${GREEN}Configs Generated${NC}\n"


# Nerd-Fonts

