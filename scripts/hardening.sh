#!/bin/bash

# Hardening script

source ./bash_colors

priv=sudo

#echo -e "${BLUE}Checking Installation of Hardening Software...${NC}"
#for name in arch-audit sysstat firejail lynis unbound apparmor \
# clamav rkhunter openresolv rng-tools opendoas 
#do
#    if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
#      then
#          echo  -e "${GREEN}Already Installed:  $name${NC}" 
#          continue
#      elif pacman -Q | grep -o "\b$name\b" | { read -r -t1 val || echo '' >/dev/null; }
#      then
#         $priv pacman -S --noconfirm --needed $name
#              if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
#                then
#                echo -e "${GREEN}Installed:  $name${NC}"
#              else
#                echo -e "${RED}Check Installation of Depenedencies${NC}"
#                exit 1
#              fi
#        else
#            echo -e "${RED}$name was not installed.${NC}"
#            exit 1
#  fi
#done
##Local
#echo
#echo -e "${GREEN}Hardening Software Installed${NC}\n"
echo -e "${BLUE}Generating Configs...${NC}"
$priv cp ../.config/my_configs/blacklist_mine.conf /etc/modprobe.d/blacklist_mine.conf
$priv unlink /etc/issue
$priv unlink /etc/issue.net
$priv cp ../.config/my_configs/issue /etc/issue
$priv cp ../.config/my_configs/issue /etc/issue.net
#$priv cp ../.config/my_configs/login.defs /etc/login.defs use a sed -i
## max=90
## min=9
## umask=027
##$priv cp ../.config/my_configs/limits.conf /etc/security/limits.conf usa a sed -i
## *   hard    core    0
#$priv cp ../.config/my_configs/profile /etc/profile
$priv cp ../.config/my_configs/sysctl.d/80-lynis.conf /etc/sysctl.d/80-lynis.conf
$priv cp ../.config/my_configs/sysctl.d/99-network.conf /etc/sysctl.d/99-network.conf
#$priv sed -i 's/nullok/nullok        rounds=65536/g' /etc/pam.d/passwd 
## No maybe toolbox
## chmod 600 /etc/at.deny
## chmod 600 /etc/cron.deny
## chmod 600 /etc/crontab
## chmod 700 /etc/cron.d
## chmod 700 /etc/cron.daily
## chmod 700 /etc/cron.hourly
## chmod 700 /etc/cron.weekly
## chmod 700 /etc/cron.monthly
#$priv sed -i 's/PermitRootLogin prohibit-password/PermitRootLogin no/g' /etc/ssh/sshd_config
#$priv chmod 600 /etc/ssh/ssh_config
#$priv chmod 600 /etc/ssh/sshd_config
#$priv chmod 600 /etc/ssh
#echo -e "${GREEN}Configs Generated${NC}\n"
#
##Extra
#echo -e "${BLUE}Starting Extra Services...${NC}"
##echo -e "${BLUE}Starting RNG Service${NC}"
##$priv systemctl enable rngd.service
##$priv systemctl start rngd.service
###rngtest -c 1000 </dev/random
#sleep 5
#echo -e "${GREEN}RNG Service Started${NC}\n"
#echo -e "${BLUE}Configuring FireJail${NC}"
#$priv firecfg
#echo -e "${GREEN}FireJail Configured${NC}\n"
#sleep 5
##echo -e "${BLUE}Starting ClamAV Service${NC}"
##$priv dnf install clamav clamd clamav-update
##$priv freshclam
##$priv systemctl --now enable clamav-freshclam.service
## crontab -e
##0 1 * * * /usr/bin/clamscan -r --quiet --move=/home/USER/infected /home/
##echo -e "${GREEN}ClamAV Service Started${NC}\n"
##echo -e "${GREEN}ClamAV Definitions Check${NC}"
##curl https://secure.eicar.org/eicar.com.txt | clamscan -
##echo -e "${GREEN}CLAMAV VALIDATED${NC}\n"
#sleep 5
#echo -e "${BLUE}Starting RKHunter Root Check${NC}"
#$priv rkhunter --update
#$priv rkhunter --propupd
#$priv cp ../.config/my_configs/rkhunter.conf.local /etc/rkhunter.conf.local
#$priv rkhunter --config-check
#echo -e "${GREEN}RKHUNTER VALIDATED${NC}\n"

