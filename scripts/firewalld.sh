#!/bin/bash

# Firewalld setup script

source ./bash_colors

echo -e "${BLUE}Firewalld setup...${NC}"
sudo firewall-cmd --set-default-zone=drop
sudo firewall-cmd --reload
sudo firewall-cmd --list-all
echo -e "${GREEN}Firewalld Service Started${NC}"
sudo nft list ruleset ;
echo -e "${GREEN}NFTABLES RULESET GENERATED${NC}\n"
