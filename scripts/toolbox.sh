#!/bin/bash

# Package install script no.1

source ./bash_colors

priv=sudo

echo -e "${BLUE}Initial Package Installation...${NC}"
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm \
                 https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm \                 --assumeyes

sudo dnf update --assumeyes
sudo dnf groupinstall "Development Tools" --assumeyes
for name in go vim exa powerline neofetch foot-terminfo linux-firmware htop \
    hwinfo shellcheck nmap android-tools gqrx urh cmake binwalk openocd pulseview stlink libsigrok \
    arm-none-eabi-gcc arm-none-eabi-newlib arm-none-eabi-binutils htop bridge-utils nodejs npm asciiquarium


# Possibly layer
# sysstat lynis rkhunter usbguard rng-tools exfat-utils  pam-u2f fontawesome5-fonts-all deepin-icon-theme pop-icon-theme
# Not Found
# ?? ghidra arm-none-eabi-gdb vde2 openbsd-netcat mono jdk17-openjdk jre-openjdk exfat-utils
# ?? install as flatpak adapta-gtk-theme arc-gtk-theme pop-gtk-theme arc-icon-theme
# Not Sure
# acct aide foobar2000 modprobed-db inxi icons-in-terminal appimagetool-bin appimagelauncher app-outlet-bin


do
    $priv dnf install $name --assumeyes
#    if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
#        then
#            echo -e "${GREEN}Already Installed:  $name${NC}"
#            continue
#        elif pacman -Q | grep -o "\b$name\b" | { read -r -t1 val || echo '' >/dev/null; }
#        then
#           $priv pacman -S --noconfirm --needed $name
#            if pacman -Q | [[ $(grep -o "\b$name\b" 2>/dev/null) ]]
#              then
#              echo -e "${GREEN}Installed:  $name${NC}"
#            else
#              echo -e "${RED}Check Installation of Depenedencies${NC}"
#            fi
#      else
#          echo -e "${RED}$name was not installed.${NC}"
#    fi
done  
echo
vim -es -u $HOME/.vimrc -i NONE -c "PlugInstall" -c "qa"
echo
echo -e "${GREEN}INSTALL COMPLETE!${NC}\n"
