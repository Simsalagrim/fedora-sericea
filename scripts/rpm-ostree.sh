#!/bin/bash

### Installed packages ###

source ./bash_colors

priv=sudo

echo -e "${BLUE}Installing Layered Packages...${NC}"
$priv sed -i 's/none/stage/g' /etc/rpm-ostreed.conf
rpm-ostree reload
systemctl enable rpm-ostreed-automatic.timer --now
rpm-ostree status
echo

# KVM-QEMU
rpm-ostree install virt-install libvirt-daemon-config-network libvirt-daemon-kvm qemu-kvm virt-manager virt-viewer guestfs-tools python3-libguestfs virt-top usbguard

echo -e "${GREEN}Packages Installed${NC}\n"
# Find Usb Devices
#for usb_ctrl in /sys/bus/pci/devices/*/usb*; do pci_path=${usb_ctrl%/*}; iommu_group=$(readlink $pci_path/iommu_group); echo "Bus $(cat $usb_ctrl/busnum) --> ${pci_path##*/} (IOMMU group ${iommu_group##*/})"; lsusb -s ${usb_ctrl#*/usb}:; echo; done
