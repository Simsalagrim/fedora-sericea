#!/bin/bash

# Source all setup scripts

source ./bash_colors

priv=sudo

echo -e "${BBLUE}Fedora Setup${NC}"
echo -e "${BLUE}Are you logged into the user you wish to run the setup for?${NC}"
select logged in Yes No
do
    case $logged in
        Yes)
            break ;;
        No)
            exit ;;
    esac
done

echo -e "${BLUE}Preparing Scripts...${NC}"
chmod 750 ./firewalld.sh ./flatpak_install.sh ./software_config.sh ./hardening.sh \
    ./rpm-ostree.sh
chmod 740 ./usbguard.sh
echo
source ./firewalld.sh
echo -e "${BLUE}Installing Packages${NC}\n"
source ./software_config.sh
source ./flatpak_install.sh
source ./rpm-ostree.sh
#source ./hardening.sh
echo -e "${GREEN}SETUP COMPLETE${NC}"
