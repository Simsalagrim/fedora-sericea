#
# ~/.bashrc
#

# If not running interactively, don't do anything
[[ $- != *i* ]] && return

# Open sway on tty1 login
#if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty1" ]; then
#  exec sway
#fi
#
## Open i3 on tty2 login
#if [ -z $DISPLAY ] && [ "$(tty)" = "/dev/tty2" ]; then
#  exec startx /usr/bin/i3
#fi

unameOut=$(uname -a)
case "${unameOut}" in
    *toolbox*)    OS="Linux toolbox";;
esac

if [ "$OS" == "Linux toolbox" ]
then
	source ~/.toolboxrc
	return
fi


unameOut=$(uname -a)
case "${unameOut}" in
    *ArchLinux*)    OS="ArchLinux toolbox";;
esac

if [ "$OS" == "ArchLinux toolbox" ]
then
	source ~/.toolboxrc
	return
fi

#Export
export TERM="xterm-256color"
export HISTCONTROL=ignoredups:erasedups
export HISTTIMEFORMAT="%h %d %H:%M:%S"
export HISTSIZE=1000
export EDITOR="vim"
export PATH=$PATH:~/.local/bin/

#Shopt
shopt -s autocd
shopt -s cdspell
shopt -s cmdhist
shopt -s histappend
shopt -s expand_aliases
shopt -s checkwinsize

#Ignore case when TAB completion
bind "set completion-ignore-case on"

#Aliases
#alias ls='exa --color=always --group-directories-first'
#alias la='exa -a --color=always --group-directories-first'
#alias ll='exa -l --color=always --group-directories-first'
alias la='ls -a'
alias ll='ls -l'
#alias lt='exa -aT --color=always --group-directories-first'
alias grep='grep --color=auto'
alias egrep='egrep --color=auto'
alias fgrep='fgrep --color=auto'
#alias pacs='sudo pacman -S'
#alias pacss='sudo pacman -Ss'
#alias pacsyu='sudo pacman -Syu'
#alias pacsyyu='sudo pacman -Syyu'
#alias yaysyu='yay -Sua --noconfirm'
#alias yays='yay -S --noconfirm'
#alias yayss='yay -Ss --noconfirm'
#alias yayrns='yay -Rns --noconfirm'
#alias yaycache='yay -Yc'
#alias pacrns='sudo pacman -Rns '
#alias paccache='sudo pacman -Sc'
#alias paclock='sudo rm /var/lib/pacman/db.lck'
alias cp="cp -i"
alias mv="mv -i"
alias rm="rm -i"
#alias vi='vim'
#alias rtfm='la /usr/share/doc/arch-wiki/html/en'
alias usbwatch='usbguard watch'
alias usballow='usbguard allow-device'
alias usbgenerate='usbguard generate-policy > /etc/usbguard/rules.conf'
#alias storemod='modprobed-db store'
#alias listmod='modprobed-db list'
alias listytdl='yt-dlp --list-formats'
alias ytdl='yt-dlp -f best'
alias ytdla='yt-dlp -x --audio-format flac'
alias dockerimages='sudo docker images'
alias dockerps='sudo docker ps'
alias dockerun='sudo docker run -it -d'
alias dockerexec='sudo docker exec -it'
alias gauth='head -n 1 .google_authenticator | xargs oathtool --totp -b $1'
alias authy='authy --no-sandbox'
alias rsynchome='sudo rsync -aAXv --delete --exclude-from=.exclude_list_rsync.txt $HOME /var/bak/Fedora_Sericea'
alias fpi='flatpak install flathub'
alias fpu='flatpak update'
alias fpr='flatpak remove'
alias fps='flatpak search'
alias fpls='flatpak list --app'
alias vim='flatpak run org.vim.Vim'

#Terminal
PS1='[\u@\H \W]\$ '
set -o vi
#neofetch
#/bin/fish
#export POWERLINE_PROMPT="hostname user_info scm python_venv ruby cwd"
#if [ -f `which powerline-daemon` ]; then
#   powerline-daemon -q
#   POWERLINE_BASH_CONTINUATION=1
#   POWERLINE_BASH_SELECT=1
#   . /usr/share/powerline/bash/powerline.sh
#fi
