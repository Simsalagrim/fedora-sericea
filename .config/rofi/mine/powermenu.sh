#!/usr/bin/env bash

# Rofi Power Menu

theme="rofi-themes-collection/theme/simple-tokyonight.rasi"
dir="$HOME/.config/rofi"
conf="/configs/powermenu.config"

uptime=$(uptime -p | sed -e 's/up //g')

rofi_command="rofi -show drun -theme $dir/$theme"

# Options
shutdown="⏻  Shutdown"
reboot="⏼  Reboot"
lock="  Lock"
suspend="⏾ Suspend"
logout="⏽ Logout"
yes="◼   Yes"
no="◼   No"
# Confirmation
confirm_exit() {
    echo -e "$confirm" | $rofi_command -p "Are You Sure?"
}

# Variable passed to rofi
options="$shutdown\n$reboot\n$lock\n$suspend\n$logout"
confirm="$yes\n$no"

chosen="$(echo -e "$options" | $rofi_command -p "Uptime: $uptime" -drun)"
case $chosen in
    $shutdown)
		ans=$(confirm_exit &)
		if [[ $ans == "◼   Yes" ]]
            then
			    systemctl poweroff
        else
			exit 0
        fi
        ;;
    $reboot)
		ans=$(confirm_exit &)
		if [[ $ans == "◼   Yes" ]]
            then
			    systemctl reboot
        else
			exit 0
        fi
        ;;
    $lock)
		if [[ -f /usr/bin/i3lock ]]; then
			i3lock
		elif [[ -f /usr/bin/betterlockscreen ]]; then
			betterlockscreen -l
		fi
        ;;
    $suspend)
		ans=$(confirm_exit &)
		if [[ $ans == "◼   Yes" ]]
            then
			    mpc -q pause
			    amixer set Master mute
			    systemctl suspend
        else
			exit 0
        fi
        ;;
    $logout)
		ans=$(confirm_exit &)
		if [[ $ans == "◼   Yes" ]]
            then
                swaymsg exit
        else
			exit 0
        fi
        ;;
esac
